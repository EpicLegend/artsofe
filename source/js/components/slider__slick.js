$(document).ready(function () {
	// START slick init

	$('.slick__category').each( function (index, element) {
		console.log(element);
		$(element).slick({
			dots: false,
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			adaptiveHeight: true,
			lazyLoad: 'ondemand',
			slidesToScroll: 1,
			nextArrow: '<div class="slider_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="14.52" height="40.137" viewBox="0 0 14.52 40.137">  <path id="Path_43" data-name="Path 43" d="M-4761.151,13226.3l-8.327,13.02-4.164,6.51,12.491,19.529" transform="translate(4774.829 -13225.759)" fill="none" stroke="#b79b6c" stroke-width="2"/></svg></div>',
			prevArrow: '<div class="slider_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="14.52" height="40.137" viewBox="0 0 14.52 40.137">  <path id="Path_43" data-name="Path 43" d="M-4761.151,13226.3l-8.327,13.02-4.164,6.51,12.491,19.529" transform="translate(4774.829 -13225.759)" fill="none" stroke="#b79b6c" stroke-width="2"/></svg></div>',
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						arrows: false,
						dots: true
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false,
						dots: true
					}
				}
			]
			
		});
	} );

	$('.looked__slick').each( function (index, element) {
		console.log(element);
		$(element).slick({
			dots: false,
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			adaptiveHeight: true,
			lazyLoad: 'ondemand',
			slidesToScroll: 1,
			nextArrow: '<div class="slider_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="14.52" height="40.137" viewBox="0 0 14.52 40.137">  <path id="Path_43" data-name="Path 43" d="M-4761.151,13226.3l-8.327,13.02-4.164,6.51,12.491,19.529" transform="translate(4774.829 -13225.759)" fill="none" stroke="#b79b6c" stroke-width="2"/></svg></div>',
			prevArrow: '<div class="slider_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="14.52" height="40.137" viewBox="0 0 14.52 40.137">  <path id="Path_43" data-name="Path 43" d="M-4761.151,13226.3l-8.327,13.02-4.164,6.51,12.491,19.529" transform="translate(4774.829 -13225.759)" fill="none" stroke="#b79b6c" stroke-width="2"/></svg></div>',
			responsive: [
				{
					breakpoint: 1160,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
						infinite: true,
						dots: true
					}
				},
				{
					breakpoint: 981,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,

					}
				}
			]
		});
	} );


	$('#product__view__slider').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		lazyLoad: 'progressive',
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '#product__view__input'
	});
	$('#product__view__input').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		lazyLoad: 'progressive',
		asNavFor: '#product__view__slider',
		dots: false,
		arrows: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					dots: false,
					arrows: false
				}
			}
		]
	});
	$('#modalItem').on('shown.bs.modal', function (event) {
		//$('#product__view__slider').slick("refresh");
		//$('#product__view__input').slick("refresh");
		$('#product__view__slider').slick("setPosition");
		$('#product__view__input').slick("setPosition");
		console.log("set slick");
    })

	// END slick init
});

