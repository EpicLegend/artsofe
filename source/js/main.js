'use strict';
//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
//= library/slick.js
//= library/jquery-ui.js

//= library/jquery.mCustomScrollbar.concat.min.js
//= library/jquery.ddslick.min.js
//= components/slider__slick.js

$(document).ready(function () {


	/* START preloader*/
	(function () {
		if ( Date.now() - preloader.timerStart >= preloader.duration  ) {
			$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
				$("#preloader-cube").css("display", "none");
			});
		} else {
			setTimeout(function () {
				$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
					$("#preloader-cube").css("display", "none");
				});
			}, preloader.duration / 1.7)
		}
	})();
	/* END preloader */

	/* START Открытие меню */
	$(".btn__menu").on("click", function () {
		$(this).toggleClass("active");
		$(".navigation__content").toggleClass("active");
	});

	$(".btn_header").on("click", function () {
		if( $(".navigation__content").hasClass("active") ) {
			$(".navigation__content").removeClass("active");
			$(".btn__menu").removeClass("active");
			$("#burger").removeClass("active");
		}
	});
	/* END откртие меню*/

	/* START красиво для hover в navigation */
	$(".navigation ul li a").hover(function () {
		var width = $(this).width();

		var elem = $(this);
		var offset = elem.offset().left - elem.parent().parent().offset().left;
		offset += parseInt( elem.css("padding-left") );

		$(".navigation__line").width( width );
		$(".navigation__line").css("left", offset);
		$(".navigation__line").css("bottom", "-6px");
	}, function () {
		// Забрать красоту! ВСЮ!
		$(".navigation__line").css("bottom", "-500%");
	});
	/* END красиво для hover в navigation */

	if ( $(window).scrollTop() > 0 ) {
        //$("header").addClass("bg-dark");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			//$("header").addClass("bg-dark");
		}  else {
			//$("header").removeClass("bg-dark");
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn-to-top").addClass("btn-to-top_active");
		} else {
			$(".btn-to-top").removeClass("btn-to-top_active");
		}

	});
	$('.btn-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
	/* END кнопка вверх */




	



	// кастомный скрол
	$(".input__select .dropdown-menu").mCustomScrollbar({
		theme: "rounded",
		keyboard:{ scrollType: "stepless" },
		scrollButtons:{enable:true}
	});



	// скрыть каталог 
	$(".catalog__burger").on("click", function(e) {
		if ( $("catalog__form__hid").is(':visible') ) {
			$(".catalog__form__hid").hide( "slow" );
		} else if( $("catalog__form__hid").is(':hidden') ) {
			$(".catalog__form__hid").show( "slow" );
		}
		
	});


	// кастомный инпут с числами
	$(".input__number").each(function(index, element){
		$(element).find(".input__number__in_plus").click(function () {
			var value = $(element).find(".input__number__cout").val();
			value = parseInt(value);
			value++;
			$(element).find(".input__number__cout").val(value);
			$(element).find(".input__number__span__cout").html(value);
		});
		$(element).find(".input__number__in_minus").click(function () {
			var value = $(element).find(".input__number__cout").val();
			value = parseInt(value);
			if(value > 0) {
				value--;

				$(element).find(".input__number__cout").val(value);
				$(element).find(".input__number__span__cout").html(value);
			}
		});
	});
	$(".input__number_item").each(function(index, element){
		$(element).find(".input__number__in_plus").click(function () {
			var value = $(element).find(".input__number__cout").val();
			value = parseInt(value);
			value++;
			$(element).find(".input__number__cout").val(value);
			$(element).find(".input__number__span__cout").html(value);
		});
		$(element).find(".input__number__in_minus").click(function () {
			var value = $(element).find(".input__number__cout").val();
			value = parseInt(value);
			if(value > 0) {
				value--;

				$(element).find(".input__number__cout").val(value);
				$(element).find(".input__number__span__cout").html(value);
			}
		});
	});

	// кастомный селекст для выбора цвета
	$(".ui-menu-item").click(function () {
        	$(this).addClass("active");
        });

	$( ".input__colora" ).selectmenu({
		classes: {
			"ui-menu-item-wrapper": "highlight"
		},
		select: function( event, ui ) {
			console.log(ui);
			$(ui.item).addClass("active");
		},
		focus: function( event, ui ) {
		}
	});
	$( ".input__sizeq" ).selectmenu({
		classes: {
			"ui-menu-item-wrapper": "highlight"
		},
		select: function( event, ui ) {
			console.log(ui);
			$(ui.item).addClass("active");
		},
		focus: function( event, ui ) {
		}
	});
	$('#input__color').ddslick({
		width: 159,
		imagePosition: "left",
		defaultSelectedIndex: null,
		height: null,
		background: "none",
		onSelected: function (data) {
			console.log(data);
		}
	});
	$('#input__size').ddslick({
		width: 159,
		imagePosition: "left",
		defaultSelectedIndex: null,
		height: null,
		background: "none",
		onSelected: function (data) {
			console.log(data);
		}
	});
});


